package kiinse.api.utilities;

import kiinse.api.utilities.format.Time;
import org.json.JSONObject;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class UtilsTest {

    private final Utils utils = new Utils();
    private final Time time = new Time();

    @Test
    public void testCheckGroup() {
        Assertions.assertTrue(Utils.isStringEmpty(""));
        Assertions.assertTrue(Utils.isStringEmpty(" "));
        Assertions.assertTrue(Utils.isStringEmpty("   "));
        Assertions.assertTrue(Utils.isStringEmpty(null));
        Assertions.assertFalse(Utils.isStringEmpty("Is not empty"));
    }

    @Test
    public void testHasLesson() {
        var trueJson = new JSONObject();
        trueJson.put("numTeacher", "Левит Л.В. (каб.412)");
        trueJson.put("denSubject", "");
        trueJson.put("numSubject", "МДК.01.05 Основы разработки веб приложений");
        trueJson.put("denTeacher", "");
        Assertions.assertTrue(utils.hasLesson(trueJson));

        var falseJson = new JSONObject();
        falseJson.put("numTeacher", "");
        falseJson.put("denSubject", "");
        falseJson.put("numSubject", "");
        falseJson.put("denTeacher", "");
        Assertions.assertFalse(utils.hasLesson(falseJson));
    }

    @Test
    public void isSubject() {
        var denJson = new JSONObject();
        denJson.put("numTeacher", "");
        denJson.put("denSubject", "Левит Л.В. (каб.412)");
        denJson.put("numSubject", "");
        denJson.put("denTeacher", "МДК.01.05 Основы разработки веб приложений");
        var numJson = new JSONObject();
        numJson.put("numTeacher", "Левит Л.В. (каб.412)");
        numJson.put("denSubject", "");
        numJson.put("numSubject", "МДК.01.05 Основы разработки веб приложений");
        numJson.put("denTeacher", "");

        if (time.isDenSubject()) {
            Assertions.assertTrue(utils.isDenSubject(denJson, Utils.messageFormat.DAILY));
            Assertions.assertFalse(utils.isDenSubject(numJson, Utils.messageFormat.DAILY));
        } else {
            Assertions.assertTrue(utils.isNumSubject(numJson, Utils.messageFormat.DAILY));
            Assertions.assertFalse(utils.isNumSubject(denJson, Utils.messageFormat.DAILY));
        }

    }
}
