package kiinse.api.utilities.format;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TextFormatTest {

    @Test
    public void testEmojiBoolean() {
        Assertions.assertEquals(TextFormat.emojiBoolean(true), "✅");
        Assertions.assertEquals(TextFormat.emojiBoolean(false), "❌");
    }
}
