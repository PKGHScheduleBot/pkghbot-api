package kiinse.api.utilities.format;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TimeTest {
    private final Time time = new Time();

    @Test
    public void testGetWeekdayNumber(){
        Assertions.assertEquals(time.getWeekdayNumber("Понедельник"), 0);
        Assertions.assertEquals(time.getWeekdayNumber("Вторник"), 1);
        Assertions.assertEquals(time.getWeekdayNumber("Среда"), 2);
        Assertions.assertEquals(time.getWeekdayNumber("Четверг"), 3);
        Assertions.assertEquals(time.getWeekdayNumber("Пятница"), 4);
        Assertions.assertEquals(time.getWeekdayNumber("Суббота"), 5);
        Assertions.assertEquals(time.getWeekdayNumber("Воскресенье"), 6);
    }

    @Test
    public void testGetRussian() {
        Assertions.assertEquals(time.getRussian("monday"), "Понедельник");
        Assertions.assertEquals(time.getRussian("tuesday"), "Вторник");
        Assertions.assertEquals(time.getRussian("wednesday"), "Среда");
        Assertions.assertEquals(time.getRussian("thursday"), "Четверг");
        Assertions.assertEquals(time.getRussian("friday"), "Пятница");
        Assertions.assertEquals(time.getRussian("saturday"), "Суббота");
        Assertions.assertEquals(time.getRussian("sunday"), "Воскресенье");
    }

    @Test
    public void testIsCyrillic() {
        Assertions.assertTrue(time.isCyrillic("Привет"));
        Assertions.assertFalse(time.isCyrillic("Hello"));
    }

    @Test
    public void testIsWeekend() {
        Assertions.assertEquals(time.isWeekend("Понедельник"), Time.weekday.WORKINGDAYS);
        Assertions.assertEquals(time.isWeekend("Вторник"), Time.weekday.WORKINGDAYS);
        Assertions.assertEquals(time.isWeekend("Среда"), Time.weekday.WORKINGDAYS);
        Assertions.assertEquals(time.isWeekend("Четверг"), Time.weekday.WORKINGDAYS);
        Assertions.assertEquals(time.isWeekend("Пятница"), Time.weekday.WORKINGDAYS);
        Assertions.assertEquals(time.isWeekend("Суббота"), Time.weekday.WEEKENDS);
        Assertions.assertEquals(time.isWeekend("Воскресенье"), Time.weekday.WEEKENDS);
    }
}
