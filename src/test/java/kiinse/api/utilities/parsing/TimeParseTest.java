package kiinse.api.utilities.parsing;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class TimeParseTest {

    private final TimeParse timeParse = new TimeParse();

    @Test
    public void testTimeFormat() {
        Assertions.assertEquals(timeParse.timeFormat("0910 - 1040"), "09:10-10:40");
    }
}
