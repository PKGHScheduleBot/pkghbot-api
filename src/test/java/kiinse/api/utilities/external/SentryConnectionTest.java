package kiinse.api.utilities.external;

import kiinse.api.utilities.external.sentry.SentryConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SentryConnectionTest {

    @Test
    public void testGetDefault() {
        var sentry = new SentryConnection();
        Assertions.assertNull(sentry.getDsn());
        Assertions.assertEquals(sentry.getRelease(), "?");
    }

    @Test
    public void testGet() {
        var sentry = new SentryConnection();
        sentry.setRelease("last release");
        sentry.setDsn("dsn-1");
        Assertions.assertEquals(sentry.getDsn(), "dsn-1");
        Assertions.assertEquals(sentry.getRelease(), "last release");
    }

}
