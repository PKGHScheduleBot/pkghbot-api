package kiinse.api.utilities.external;

import kiinse.api.utilities.external.rest.RestConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RestConnectionTest {

    @Test
    public void testGetDefault() {
        var rest = new RestConnection();
        Assertions.assertEquals(rest.getLogin(), "root");
        Assertions.assertEquals(rest.getPort(), 8080);
        Assertions.assertEquals(rest.getBotVersion(), "?");
        Assertions.assertEquals(rest.getApiVersion(), "?");
        Assertions.assertEquals(rest.getPassword(), "root");
    }

    @Test
    public void testGet() {
        var rest = new RestConnection();
        rest.setPort(1337);
        rest.setLogin("kiinse");
        rest.setPassword("password");
        rest.setBotVersion("bot last");
        rest.setApiVersion("api last");
        Assertions.assertEquals(rest.getLogin(), "kiinse");
        Assertions.assertEquals(rest.getPort(), 1337);
        Assertions.assertEquals(rest.getBotVersion(), "bot last");
        Assertions.assertEquals(rest.getApiVersion(), "api last");
        Assertions.assertEquals(rest.getPassword(), "password");
    }

}
