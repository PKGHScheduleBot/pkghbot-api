package kiinse.api.utilities.external;

import kiinse.api.utilities.external.sql.SQLConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class SQLConnectionTest {

    @Test
    public void testGetDefault() {
        var settings = new SQLConnection();
        Assertions.assertEquals(settings.getHost(), "localhost");
        Assertions.assertEquals(settings.getLogin(), "postgres");
        Assertions.assertEquals(settings.getPort(), "5432");
        Assertions.assertEquals(settings.getPassword(), "postgres");
        Assertions.assertEquals(settings.getDbName(), "pkghbot");
    }

    @Test
    public void testGet() {
        var settings = new SQLConnection();
        settings.setPort("1337");
        settings.setLogin("kiinse");
        settings.setPassword("password");
        settings.setHost("127.0.0.1");
        settings.setDbName("pkghbot");
        Assertions.assertEquals(settings.getHost(), "127.0.0.1");
        Assertions.assertEquals(settings.getLogin(), "kiinse");
        Assertions.assertEquals(settings.getPort(), "1337");
        Assertions.assertEquals(settings.getPassword(), "password");
        Assertions.assertEquals(settings.getDbName(), "pkghbot");
    }
}
