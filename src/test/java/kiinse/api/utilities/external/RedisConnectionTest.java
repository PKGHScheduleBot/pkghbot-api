package kiinse.api.utilities.external;

import kiinse.api.utilities.external.redis.RedisConnection;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

public class RedisConnectionTest {

    @Test
    public void testGetDefault() {
        var redis = new RedisConnection();
        Assertions.assertNull(redis.getPassword());
        Assertions.assertEquals(redis.getHost(), "localhost");
        Assertions.assertEquals(redis.getPort(), "6379");
        Assertions.assertEquals(redis.getNodeID(), "node-1");
        Assertions.assertEquals(redis.getWriters(), "jackson");
        Assertions.assertEquals(redis.getReaders(), "jackson");
    }

    @Test
    public void testGet() {
        var redis = new RedisConnection();
        redis.setHost("127.0.0.1");
        redis.setPort("1337");
        redis.setPassword("password");
        redis.setNodeID("RequestNode");
        redis.setReaders("json");
        redis.setWriters("json");
        Assertions.assertEquals(redis.getHost(), "127.0.0.1");
        Assertions.assertEquals(redis.getPort(), "1337");
        Assertions.assertEquals(redis.getNodeID(), "RequestNode");
        Assertions.assertEquals(redis.getWriters(), "json");
        Assertions.assertEquals(redis.getReaders(), "json");
        Assertions.assertEquals(redis.getPassword(), "password");
    }
}
