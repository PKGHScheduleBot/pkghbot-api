package kiinse.api.utilities.external.sql;

import kiinse.api.utilities.Utils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Класс настроек для подключения к SQL
 * @since 2.2.0
 * @version 2.3.0
 */
@Slf4j
public class SQLConnection {

    @Getter
    private String host = "localhost";

    @Getter
    private String port = "5432";

    @Getter
    private String login = "postgres";

    @Getter
    private String password = "postgres";

    @Getter
    private String dbName = "pkghbot";

    /**
     * Setter для SQL host
     * Default = localhost
     * @param sqlHost Хост базы данных
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setHost(String sqlHost) throws IllegalArgumentException {
        if (Utils.isStringEmpty(sqlHost)) {
            throw new IllegalArgumentException("SQL host is empty");
        }
        this.host = sqlHost;
        log.info("SQL host set: {}", this.host);
    }

    /**
     * Setter для SQL port
     * Default = 3306
     * @param sqlPort Порт базы данных
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setPort(String sqlPort) throws IllegalArgumentException {
        if (Utils.isStringEmpty(sqlPort)) {
            throw new IllegalArgumentException("SQL port is empty");
        }
        this.port = sqlPort;
        log.info("SQL port set: {}", this.port);
    }

    /**
     * Setter для SQL login
     * Default = root
     * @param sqlLogin Логин базы данных
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setLogin(String sqlLogin) throws IllegalArgumentException {
        if (Utils.isStringEmpty(sqlLogin)) {
            throw new IllegalArgumentException("SQL login is empty");
        }
        this.login = sqlLogin;
        log.info("SQL login set: {}", this.login);
    }

    /**
     * Setter для SQL password
     * Default = root
     * @param sqlPassword Пароль базы данных
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setPassword(String sqlPassword) throws IllegalArgumentException {
        if (Utils.isStringEmpty(sqlPassword)) {
            throw new IllegalArgumentException("SQL password is empty");
        }
        this.password = sqlPassword;
        log.info("SQL password set: {}", this.password.charAt(0) + "*****");
    }

    /**
     * Setter для SQL dataBase name
     * Default = pkghbot
     * @param sqldbName Имя базы данных
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setDbName(String sqldbName) throws IllegalArgumentException {
        if (Utils.isStringEmpty(sqldbName)) {
            throw new IllegalArgumentException("SQL database name is empty");
        }
        this.dbName = sqldbName;
        log.info("SQL db name set: {}", this.dbName);
    }
}
