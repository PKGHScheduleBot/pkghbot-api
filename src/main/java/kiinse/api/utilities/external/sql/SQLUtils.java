package kiinse.api.utilities.external.sql;

import kiinse.api.utilities.external.sql.database.tables.Banlist;
import kiinse.api.utilities.external.sql.database.tables.Users;
import lombok.extern.slf4j.Slf4j;
import org.jooq.Record;
import org.jooq.impl.DSL;
import java.util.ArrayList;
import java.util.List;

/**
 * Класс получения данных из Базы Данных MySQL
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.3.3
 */
@Slf4j
public class SQLUtils {

    /**
     * Данный метод вносит в БД информацию о пользователе при первом запуске
     * @param chat String значение ID чата пользователя
     * @param username Username пользователя
     */
    public static void firstMessage(String chat, String username) {
        var record = SQL.getContext().newRecord(Users.USERS);
        record.setChat(chat);
        record.setUsername(username);
        record.store();
    }

    /**
     * Метод для получения количества строк в БД
     * @return Количество строк в БД
     */
    public static int totalChats() {
        return SQL.getContext().fetchCount(Users.USERS);
    }

    /**
     * Метод для получения количества активных пользователей в БД
     * @return Количество строк в БД
     */
    public static int totalActiveChats() {
        return SQL.getContext().fetchCount(Users.USERS, Users.USERS.STATUS.equal("АКТИВНЫЙ"));
    }

    /**
     * Метод для получения количества строк со значением "БОТ ЗАБЛОКИРОВАН" в колонке "GroupName"
     * @return Количество строк
     */
    public static int totalBlockedChats() {
        return SQL.getContext().fetchCount(Users.USERS, Users.USERS.STATUS.equal("БОТ ЗАБЛОКИРОВАН"));
    }

    /**
     * Метод для получения количества строк со значением "ГРУППА" в колонке "GroupName"
     * @return Количество строк
     */
    public static int totalGroupChats() {
        return SQL.getContext().fetchCount(Users.USERS, Users.USERS.USERNAME.equal("ГРУППА"));
    }

    /**
     * Метод для получения количества администраторов
     * @return Количество строк
     */
    public static int totalAdmins() {
        return SQL.getContext().fetchCount(Users.USERS, Users.USERS.ISADMIN.equal(true));
    }

    /**
     * Метод для обновления группы у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @param group Новое значения колонки "GroupName"
     */
    public static void updateGroup(String chat, String group) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.GROUPNAME, group)
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод для обновления статуса (Вкл/Выкл) вечерней рассылки у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @param mailing Новое Boolean значения колонки "isAutoMailing"
     */
    public static void updateMailing(String chat, Boolean mailing) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.ISAUTOMAILING, mailing)
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод для обновления статуса (Вкл/Выкл) рассылки пар у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @param mailing Новое Boolean значения колонки "isNextLessonMailing"
     */
    public static void updateNextLessonMailing(String chat, Boolean mailing) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.ISNEXTLESSONMAILING, mailing)
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод для обновления статуса (Вкл/Выкл) рассылки замен у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @param mailing Новое Boolean значения колонки "isNextLessonMailing"
     */
    public static void updateChangesMailing(String chat, Boolean mailing) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.ISCHANGESMAILING, mailing)
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод обновления статуса администратора у указанного пользователя в БД
     * @param username Username пользователя
     * @param admin Новое Boolean значения колонки "isAdmin"
     */
    public static void updateAdmin(String username, Boolean admin) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.ISADMIN, admin)
                .where(Users.USERS.USERNAME.equal(username))
                .execute();
    }

    /**
     * Метод обновления статуса административных рассылок у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @param mailing Новое Boolean значения колонки "isAdmin"
     * @since 1.0.3
     */
    public static void updateAdminMailing(String chat, Boolean mailing) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.ISADMINMAILING, mailing)
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод получения статуса рассылки пар у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если рассылка включена и False если нет
     */
    public static Boolean isNextLessonMailing(String chat) {
        return SQL.getContext().select(Users.USERS.ISNEXTLESSONMAILING)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
    }

    /**
     * Метод получения значения "GroupName" у указанного Чата в БД
     * @param chat String значение ID чата пользователя
     * @return Возвращает значение строки "GroupName"
     */
    public static String getGroup(String chat) {
        return SQL.getContext().select(Users.USERS.GROUPNAME)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
    }

    /**
     * Метод проверки пользователя в БД на то, является ли он групповым чатом
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если у пользователя в строке "UserName" значение "ГРУППА"
     */
    public static Boolean isGroupChat(String chat) {
        var result = SQL.getContext().select(Users.USERS.USERNAME)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch();
        return !result.isEmpty() && result.get(0).component1().equals("ГРУППА");
    }

    /**
     * Метод получения статуса вечерней рассылки у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если рассылка включена и False если нет
     */
    public static Boolean isMailing(String chat) {
        return SQL.getContext().select(Users.USERS.ISAUTOMAILING)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
    }

    /**
     * Метод получения статуса уведомления о заменах у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если рассылка включена и False если нет
     */
    public static Boolean isChangesMailing(String chat) {
        return SQL.getContext().select(Users.USERS.ISCHANGESMAILING)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
    }

    /**
     * Метод проверки группы пользователя в БД на соответствие получаемой
     * @param chat String значение ID чата пользователя
     * @param group Получаемая группа
     * @return Возвращает True если получаемая группа и группа пользователя одинаковы
     */
    public static Boolean isUserGroup(String chat, String group) {
        var result = SQL.getContext().select(Users.USERS.GROUPNAME)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
        return result != null && result.equals(group);
    }

    /**
     * Метод получения статуса административных уведомлений у указанного пользователя в БД
     *
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если рассылка включена и False если нет
     */
    public static Boolean isAdminMailing(String chat) {
        return SQL.getContext().select(Users.USERS.ISADMINMAILING)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
    }

    /**
     * Метод получения статуса администратора у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если пользователь администратор и False если нет
     */
    public static Boolean isAdmin(String chat) {
        return SQL.getContext().select(Users.USERS.ISADMIN)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
    }

    /**
     * Метод получения отформатированного списка администраторов из БД
     * @return Отформатированный текст, готовый для отправки
     */
    public static StringBuilder adminList() {
        var msg = new StringBuilder("▬▬▬ Админы ▬▬▬");
        var query = SQL.getContext().select(Users.USERS.fields())
                .from(Users.USERS)
                .where(Users.USERS.ISADMIN.equal(true)).fetch();
        for (Record record : query) {
            msg.append("\n")
                    .append(record.getValue(Users.USERS.ID))
                    .append(" | ")
                    .append(record.getValue(Users.USERS.CHAT))
                    .append(" | ")
                    .append(record.getValue(Users.USERS.GROUPNAME))
                    .append(" | @")
                    .append(record.getValue(Users.USERS.USERNAME));
        }
        return msg;
    }

    /**
     * Метод получения списка администраторов из БД
     * @return Список администраторов
     */
    public static List<String> allAdminsList() {
        var list = new ArrayList<String>();
        var query = SQL.getContext().select(Users.USERS.fields())
                .from(Users.USERS)
                .where(Users.USERS.ISADMIN.equal(true)).fetch();
        for (Record record : query) {
            list.add(record.getValue(Users.USERS.CHAT));
        }
        return list;
    }

    /**
     * Метод проверки пользователя в БД на то, заблокировал ли он бота
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если у пользователя в строке "UserName" значение "БОТ ЗАБЛОКИРОВАН"
     */
    public static Boolean isBlocked(String chat) {
        var result = SQL.getContext().select(Users.USERS.STATUS)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
        return result.equals("БОТ ЗАБЛОКИРОВАН") || result.equals("ДЕАКТИВИРОВАН");
    }

    /**
     * Метод для установки значения "БОТ ЗАБЛОКИРОВАН" в "Status" у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     */
    public static void setBlocked(String chat) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.STATUS, "БОТ ЗАБЛОКИРОВАН")
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод для установки значения "ДЕАКТИВИРОВАН" в "Status" у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     */
    public static void setDeactivated(String chat) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.STATUS, "ДЕАКТИВИРОВАН")
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод для установки значения в "Status" у указанного пользователя в БД
     *
     * @param chat String значение ID чата пользователя
     */
    public static void setStatus(String chat, String status) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.STATUS, status)
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод проверки пользователя на наличие в БД по username
     * @param user Username пользователя
     * @return Возвращает True если такой пользователь существует в БД
     */
    public static Boolean hasUserByName(String user) {
        return SQL.getContext().select(Users.USERS.fields())
                .from(Users.USERS)
                .where(Users.USERS.USERNAME.equal(user))
                .fetch().isNotEmpty();
    }

    /**
     * Метод проверки пользователя на наличие в БД по ID чата
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если такой пользователь существует в БД
     */
    public static Boolean hasUserByChat(String chat) {
        return SQL.getContext().select(Users.USERS.fields())
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().isNotEmpty();
    }

    /**
     * Метод получения значения "UserName" у указанного Чата в БД
     * @param chat String значение ID чата пользователя
     * @return Возвращает значение строки "UserName"
     */
    public static String getUserName(String chat) {
        return SQL.getContext().select(Users.USERS.USERNAME)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1();
    }

    /**
     * Метод получения ID чата у пользователя под определённым номером
     * @param id Номер строки в БД
     * @return Возвращает значение строки "Chat"
     */
    public static String getChat(int id) {
        return SQL.getContext().select(Users.USERS.CHAT)
                .from(Users.USERS)
                .where(Users.USERS.ID.equal(id))
                .fetch().get(0).component1();
    }

    /**
     * Метод для обновления username у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     * @param username Новое значения колонки "UserName"
     */
    public static void updateUserName(String chat, String username) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.USERNAME, username)
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод для установки значения "РЕГИСТРАЦИЯ" в "Status" у указанного пользователя в БД
     * @param chat String значение ID чата пользователя
     */
    public static void setUnspecified(String chat) {
        SQL.getContext().update(Users.USERS)
                .set(Users.USERS.STATUS, "РЕГИСТРАЦИЯ")
                .where(Users.USERS.CHAT.equal(chat))
                .execute();
    }

    /**
     * Метод проверки пользователя в БД на установленную группу
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если у пользователя в строке "GroupName" значение "НЕ УКАЗАНО"
     */
    public static Boolean isUnspecified(String chat) {
        return SQL.getContext().select(Users.USERS.STATUS)
                .from(Users.USERS)
                .where(Users.USERS.CHAT.equal(chat))
                .fetch().get(0).component1().equals("РЕГИСТРАЦИЯ");
    }

    /**
     * Метод проверки пользователя в БД на то, заблокирован ли он ботом
     * @param chat String значение ID чата пользователя
     * @return Возвращает True если пользователь находится в таблице БД "banlist"
     */
    public static Boolean isBanned(String chat) {
        return SQL.getContext().select(Banlist.BANLIST.fields())
                .from(Banlist.BANLIST)
                .where(Banlist.BANLIST.CHAT.equal(chat))
                .fetch().isNotEmpty();
    }

    /**
     * Метод для добавления пользователя в таблицу БД "banlist"
     * @param chat String значение ID чата пользователя
     * @param time Срок бана в ms (Long)
     */
    public static void giveBan(String chat, Long time) {
        var record = SQL.getContext().newRecord(Banlist.BANLIST);
        record.setChat(chat);
        record.setBantime(time);
        record.store();
        setStatus(chat, "ЗАБЛОКИРОВАН");
    }

    /**
     * Метод получения значения "BanTime" у указанного Чата в БД
     * @param id Номер строки в БД
     * @return Возвращает значение строки "BanTime"
     */
    public static Long getBanTime(int id) {
        return SQL.getContext().select(Banlist.BANLIST.BANTIME)
                .from(Banlist.BANLIST)
                .where(Banlist.BANLIST.ID.equal(id))
                .fetch().get(0).component1();
    }

    /**
     * Метод для удаления пользователя из таблицы БД "banlist"
     * @param id Номер строки в БД
     */
    public static void unBan(int id) {
        var chat = SQL.getContext().select(Banlist.BANLIST.CHAT)
                .from(Banlist.BANLIST)
                .where(Banlist.BANLIST.ID.equal(id))
                .fetch().get(0).component1();
        setStatus(chat, "АКТИВНЫЙ");
        SQL.getContext().deleteFrom(Banlist.BANLIST).where(Banlist.BANLIST.ID.equal(id)).execute();
    }

    /**
     * Метод для получения количества строк в таблице "banlist"
     * @return Количество строк
     */
    public static int totalBans() {
        return SQL.getContext().fetchCount(Banlist.BANLIST);
    }

    /**
     * Метод для получения наибольшего ID в таблице "banlist"
     * @return Наибольшее ID
     */
    public static int totalMaxBans() {
        var result = SQL.getContext().select(DSL.max(Banlist.BANLIST.ID))
                .from(Banlist.BANLIST).fetch();
        if (!result.isEmpty()) {
            result.get(0).component1();
        }
        return 0;
    }

    /**
     * Метод получения ID чата в таблице "banlist" по id строки
     * @param id Номер строки в БД
     * @return Возвращает значение строки "Chat"
     */
    public static String getBanChat(int id) {
        return SQL.getContext().select(Banlist.BANLIST.CHAT)
                .from(Banlist.BANLIST)
                .where(Banlist.BANLIST.ID.equal(id))
                .fetch().get(0).component1();
    }

    /**
     * Метод для получения количества строк со значением True в колонке "isAutoMailing"
     * @return Количество строк
     */
    public static int totalAutoMailing() {
        return SQL.getContext().fetchCount(Users.USERS, Users.USERS.ISAUTOMAILING.equal(true), Users.USERS.STATUS.equal("АКТИВНЫЙ"));
    }

    /**
     * Метод для получения количества строк со значением True в колонке "isAutoMailing"
     * @return Количество строк
     */
    public static int totalChangesMailing() {
        return SQL.getContext().fetchCount(Users.USERS, Users.USERS.ISCHANGESMAILING.equal(true), Users.USERS.STATUS.equal("АКТИВНЫЙ"));
    }

    /**
     * Метод для получения количества строк со значением True в колонке "isNextLessonMailing"
     * @return Количество строк
     */
    public static int totalNextLessonMailing() {
        return SQL.getContext().fetchCount(Users.USERS, Users.USERS.ISNEXTLESSONMAILING.equal(true), Users.USERS.STATUS.equal("АКТИВНЫЙ"));
    }
}
