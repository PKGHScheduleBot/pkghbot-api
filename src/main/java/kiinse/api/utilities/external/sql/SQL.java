package kiinse.api.utilities.external.sql;

import kiinse.api.utilities.external.sql.database.tables.Banlist;
import kiinse.api.utilities.external.sql.database.tables.Users;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.jooq.DSLContext;
import org.jooq.SQLDialect;
import org.jooq.impl.DSL;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.Properties;

/**
 * Класс подключения к БД postgresql
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.3.3
 */
@Slf4j
public class SQL {


    /** Свойство, хранящее в себе подключение в БД */
    @Getter
    private static Connection connection;

    /** Свойство, хранящее в себе DSLContext для работы с таблицами {@link DSLContext} */
    @Getter
    private static DSLContext context;

    /** Свойство, хранящее данные для подключения SQL {@link SQLConnection} */
    @Getter
    private static SQLConnection SQLSettings;

    /** Инициализация настроек для подключения {@link SQLConnection} */
    public SQL(SQLConnection settings) {
        SQL.SQLSettings = settings;
    }

    /**
     * Метод для подключения MySQL
     * @throws SQLException в случае если не удалось подключиться к базе данных
     */
    public void connect() throws SQLException {
        if (!isConnected()) {
            log.info("Connecting to database...");
            connectToDataBase(getURL(), getProperties());
            log.info("Database connected");
        } else {
            throw new SQLException("DataBase already connected");
        }

    }

    /**
     * Метод получения URL строки для подключения к базе данных
     * @return Возвращает строку с данными из SQLConnection вида: jdbc:postgresql://host:port/database {@link SQLConnection}
     */
    private String getURL() {
        return "jdbc:postgresql://" + SQL.getSQLSettings().getHost() + ":" + SQL.getSQLSettings().getPort() + "/" + SQL.getDataBaseName();
    }

    /**
     * Получение Properties для подключения из SQLConnection
     * @return Properties для подключения
     */
    private Properties getProperties() {
        var connInfo = new Properties();
        connInfo.setProperty("user", SQL.getSQLSettings().getLogin());
        connInfo.setProperty("password", SQL.getSQLSettings().getPassword());
        connInfo.setProperty("useUnicode", "true");
        connInfo.setProperty("characterEncoding", "UTF-8");
        return connInfo;
    }

    /**
     * Подключение к базе данных бота и создание таблиц при их отсутствии
     * @param url URL для подключения
     * @param connInfo Properties для подключения
     * @throws SQLException в случае если не удалось подключиться к базе данных
     */
    private void connectToDataBase(String url, Properties connInfo) throws SQLException {
        System.getProperties().setProperty("org.jooq.no-logo", "true");
        System.getProperties().setProperty("org.jooq.no-tips", "true");
        SQL.connection = DriverManager.getConnection(url, connInfo);
        context = DSL.using(SQL.connection, SQLDialect.POSTGRES);
        context.createTableIfNotExists(Banlist.BANLIST)
                .columns(Banlist.BANLIST.ID)
                .columns(Banlist.BANLIST.CHAT)
                .columns(Banlist.BANLIST.BANTIME)
                .primaryKey(Banlist.BANLIST.ID)
                .execute();

        context.createTableIfNotExists(Users.USERS)
                .columns(Users.USERS.ID)
                .columns(Users.USERS.CHAT)
                .columns(Users.USERS.GROUPNAME)
                .columns(Users.USERS.USERNAME)
                .columns(Users.USERS.STATUS)
                .columns(Users.USERS.ISAUTOMAILING)
                .columns(Users.USERS.ISNEXTLESSONMAILING)
                .columns(Users.USERS.ISCHANGESMAILING)
                .columns(Users.USERS.ISADMINMAILING)
                .columns(Users.USERS.ISADMIN)
                .primaryKey(Users.USERS.ID)
                .execute();
    }

    /**
     * Метод для получения имени базы данных
     * @return Возвращает имя БД
     */
    public static String getDataBaseName() {
        return SQLSettings.getDbName();
    }

    /**
     * Метод для проверки соединения MySQL
     * @return Возвращает True если connection не является null {@link Connection}
     */
    public static boolean isConnected() {
        return (connection != null);
    }
}
