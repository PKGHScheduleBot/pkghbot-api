package kiinse.api.utilities.external.redis;

import io.datatree.Promise;
import io.datatree.Tree;
import io.sentry.Sentry;
import kiinse.api.utilities.Utils;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import services.moleculer.ServiceBroker;
import services.moleculer.context.CallOptions;
import services.moleculer.transporter.RedisTransporter;

/**
 * Класс общения с парсером с помощью Moleculer
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.2.1
 */
@Slf4j
public class Redis {

    /** Свойство, хранящее в себе брокер {@link ServiceBroker} */
    @Getter
    private static ServiceBroker redisBroker = null;


    /** Свойство, хранящее в себе redis transporter {@link RedisTransporter} */
    @Getter
    private static RedisTransporter redisTransporter = null;

    /** Свойство, хранящее данные для подключения Redis {@link RedisConnection} */
    @Getter
    @Setter
    private static RedisConnection redisConnection = null;

    /**
     * Метод создания Redis подключения в Service Broker {@link ServiceBroker}
     * @param redis Данные для подключения Redis {@link RedisConnection}
     * @throws IllegalStateException В случае если RedisConnection null {@link RedisConnection}
     */
    public void createRedis(RedisConnection redis) throws Exception {
        setRedisConnection(redis);
        if (getRedisConnection() != null) {
            log.info("Started creating a Redis Transporter...");
            var host = "redis://" + getRedisConnection().getHost() + ":" + getRedisConnection().getPort();
            redisTransporter = new RedisTransporter(host);
            if (!Utils.isStringEmpty(getRedisConnection().getPassword())) {
                redisTransporter.setPassword(getRedisConnection().getPassword());
            }
            Redis.redisBroker = ServiceBroker.builder()
                    .nodeID(getRedisConnection().getNodeID())
                    .transporter(redisTransporter)
                    .readers(getRedisConnection().getReaders())
                    .writers(getRedisConnection().getWriters())
                    .build();

            Redis.redisBroker.start();
            log.info("Redis Transporter created");
        } else {
            throw new IllegalStateException("RedisConnection is null");
        }
    }

    /**
     * Метод для отправки запроса с именем группы в парсер.
     * Если в настройках указан резервный хост Redis на котором должен быть запущен парсер, то при ошибке произойдёт подключение к нему.
     * @param broker Брокер с подключением к Redis серверу {@link ServiceBroker}
     * @param group Имя группы для запроса расписания
     * @return Возвращает Расписание группы на всю неделю с сайта колледжа в формате Tree
     */
    private Promise getGroup(ServiceBroker broker, String group) {
        CallOptions.nodeID("ApiService").timeout(1500).retryCount(3);
        return Redis.redisBroker.waitForServices("main").then(rsp -> {
            Tree params = new Tree().put("name", group);
            return broker.call("main.parsedata", params);
        });
    }

    /**
     * Отправляет запросы через Redis сервер парсеру с помощью метода getGroup.
     * Если при первом запросе полученное значение null, то отправляется ещё один запрос для убеждения.
     *
     * @param group Имя группы
     * @return Возвращает в случае успеха JSON объект с расписанием группы
     */
    public JSONObject getSchedule(String group) {
        int count = 0;
        Tree tree = null;
        do {
            count++;
            try {
                tree = getGroup(redisBroker, group).waitFor();
            } catch (Exception e) {
                log.error("An error occurred while getting the schedule: {}", e.getMessage());
                Sentry.captureException(e);
            }
            if (count == 2) {
                break;
            }
        } while (tree == null);
        log.debug("Group {} schedule received: {}", group, tree == null ? null : tree.toString());
        return tree == null ? null : new JSONObject(tree.toString());
    }

    /**
     * Закрывает текущее подключение к Redis серверу.
     */
    public void closeConnection() {
        redisBroker.stop();
        redisBroker = null;
    }
}
