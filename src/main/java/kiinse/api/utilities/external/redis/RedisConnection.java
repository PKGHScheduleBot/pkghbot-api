package kiinse.api.utilities.external.redis;

import kiinse.api.utilities.Utils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Класс настроек для подключения к Redis
 * @since 2.2.0
 * @version 2.2.0
 */
@Slf4j
public class RedisConnection {

    @Getter
    private String host = "localhost";

    @Getter
    private String port = "6379";

    @Getter
    private String nodeID = "node-1";

    @Getter
    private String password = null;

    @Getter
    private String writers = "jackson";

    @Getter
    private String readers = "jackson";

    /**
     * Setter для Redis password
     * Default = null
     * @param redisPassword Имя пользователя
     */
    public void setPassword(String redisPassword) {
        if (!Utils.isStringEmpty(redisPassword)) {
            this.password = redisPassword;
        }
        log.info("Redis password set: {}", this.password.charAt(0) + "*****");
    }

    /**
     * Setter для Redis host
     * Default = localhost
     * @param redisHost Хост
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setHost(String redisHost) throws IllegalArgumentException {
        if (Utils.isStringEmpty(redisHost)) {
            throw new IllegalArgumentException("Redis host is empty");
        }
        this.host = redisHost;
        log.info("Redis host set: {}", this.host);
    }

    /**
     * Setter для Redis port
     * Default = 6379
     * @param redisPort Порт
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setPort(String redisPort) throws IllegalArgumentException {
        if (Utils.isStringEmpty(redisPort)) {
            throw new IllegalArgumentException("Redis port is empty");
        }
        this.port = redisPort;
        log.info("Redis port set: {}", this.port);
    }

    /**
     * Setter для Redis nodeID
     * Default = node-1
     * @param redisNodeID nodeID
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setNodeID(String redisNodeID) throws IllegalArgumentException {
        if (Utils.isStringEmpty(redisNodeID)) {
            throw new IllegalArgumentException("Redis nodeID is empty");
        }
        this.nodeID = redisNodeID;
        log.info("Redis nodeID set: {}", this.nodeID);
    }

    /**
     * Setter для Redis writers
     * Default = jackson
     * @param redisWriters Хост
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setWriters(String redisWriters) throws IllegalArgumentException {
        if (Utils.isStringEmpty(redisWriters)) {
            throw new IllegalArgumentException("Redis writers is empty");
        }
        this.writers = redisWriters;
        log.info("Redis writers set: {}", this.writers);
    }

    /**
     * Setter для Redis readers
     * Default = jackson
     * @param redisReaders Хост
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setReaders(String redisReaders) throws IllegalArgumentException {
        if (Utils.isStringEmpty(redisReaders)) {
            throw new IllegalArgumentException("Redis readers is empty");
        }
        this.readers = redisReaders;
        log.info("Redis readers set: {}", this.readers);
    }
}
