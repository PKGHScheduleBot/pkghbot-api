package kiinse.api.utilities.external.rest;

import kiinse.api.utilities.external.rest.Services.PKGHService;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import services.moleculer.ServiceBroker;
import services.moleculer.web.ApiGateway;
import services.moleculer.web.middleware.BasicAuthenticator;
import services.moleculer.web.netty.NettyServer;
import services.moleculer.web.router.Route;

/**
 * Класс общения через Rest с помощью Moleculer
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.2.1
 */
@Slf4j
public class Rest {

    /** Свойство, хранящее в себе брокер {@link ServiceBroker} */
    private static ServiceBroker restBroker = null;

    /** Свойство, хранящее данные для подключения Rest {@link RestConnection} */
    @Getter
    @Setter
    private static RestConnection restConnection = null;

    /**
     * Метод создания Rest подключения в Service Broker {@link ServiceBroker}
     * @param rest Данные для подключения Rest (Может быть null) {@link RestConnection}
     * @throws IllegalStateException В случае если RestConnection null {@link RestConnection}
     */
    public void createRest(RestConnection rest) throws Exception {
        setRestConnection(rest);
        if (getRestConnection() != null) {
            log.info("Started creating a REST services...");
            restBroker = new ServiceBroker();
            var apiGateway = new ApiGateway();
            var route = new Route();
            route.use(new BasicAuthenticator(getRestConnection().getLogin(), getRestConnection().getPassword()));
            route.addToWhiteList("**");
            apiGateway.addRoute(route);
            Rest.restBroker
                    .createService(new NettyServer(getRestConnection().getPort()))
                    .createService(new PKGHService())
                    .createService(apiGateway)
                    .start();
            log.info("Rest services created");
        } else {
            throw new IllegalStateException("RestConnection is null");
        }
    }
}
