package kiinse.api.utilities.external.rest;

import kiinse.api.utilities.Utils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Класс настроек для подключения к Rest
 * @since 2.2.1
 * @version 2.2.3
 */
@Slf4j
public class RestConnection {

    @Getter
    private String login = "root";

    @Getter
    private String password = "root";

    @Getter
    private int port = 8080;

    @Getter
    private String botVersion = "?";

    @Getter
    private String apiVersion = "?";

    /**
     * Setter для Rest login
     * Default = root
     * @param restLogin Логин
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setLogin(String restLogin) throws IllegalArgumentException {
        if (Utils.isStringEmpty(restLogin)) {
            throw new IllegalArgumentException("Rest login is empty");
        }
        this.login = restLogin;
        log.info("Rest login set: {}", this.login);
    }

    /**
     * Setter для Rest password
     * Default = root
     * @param restPassword Пароль
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setPassword(String restPassword) throws IllegalArgumentException {
        if (Utils.isStringEmpty(restPassword)) {
            throw new IllegalArgumentException("Rest password is empty");
        }
        this.password = restPassword;
        log.info("Rest password set: {}", this.password.charAt(0) + "*****");
    }

    /**
     * Setter для Rest port
     * Default = 8080
     * @param restPort Порт
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setPort(int restPort) throws IllegalArgumentException {
        if (Utils.isStringEmpty(String.valueOf(restPort))) {
            throw new IllegalArgumentException("Rest port is empty");
        }
        this.port = restPort;
        log.info("Rest port set: {}", this.port);
    }

    /**
     * Setter для Rest bot version
     * Default = ?
     * @param restBotVersion Имя сервиса
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setBotVersion(String restBotVersion) throws IllegalArgumentException {
        if (Utils.isStringEmpty(restBotVersion)) {
            throw new IllegalArgumentException("Bot version is empty");
        }
        this.botVersion = restBotVersion;
        log.info("Rest bot version set: {}", this.botVersion);
    }

    /**
     * Setter для Rest bot api version
     * Default = ?
     * @param restApiVersion Имя сервиса
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setApiVersion(String restApiVersion) throws IllegalArgumentException {
        if (Utils.isStringEmpty(restApiVersion)) {
            throw new IllegalArgumentException("Bot API version is empty");
        }
        this.apiVersion = restApiVersion;
        log.info("Rest API version set: {}", this.apiVersion);
    }

}
