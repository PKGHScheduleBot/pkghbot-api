package kiinse.api.utilities.external.rest.Services;

import kiinse.api.utilities.external.redis.Redis;
import kiinse.api.utilities.external.rest.RestActionFactory;
import lombok.extern.slf4j.Slf4j;
import services.moleculer.service.Action;
import services.moleculer.service.Name;
import services.moleculer.service.Service;

/**
 * Класс сервиса Rest
 *
 * @author kiinse
 * @since 2.2.3
 * @version 2.2.3
 */
@Slf4j
@Name("pkghbot")
public class PKGHService extends Service {

    private final RestActionFactory restActionFactory = new RestActionFactory();

    Action sql = ctx -> {
        log.info("A sql service call has occurred");
        return restActionFactory.getSQL();
    };
    Action redis = ctx -> {
        log.info("A redis service call has occurred");
        return restActionFactory.getRedis(Redis.getRedisBroker(), Redis.getRedisTransporter());
    };
    Action bot = ctx -> {
        log.info("A bot service call has occurred");
        return restActionFactory.getBot();
    };
    Action changes = ctx -> {
        log.info("A changes service call has occurred");
        return restActionFactory.getChanges();
    };
    Action time = ctx -> {
        log.info("A time service call has occurred");
        return restActionFactory.getTime();
    };
    Action schedule = ctx -> {
        log.info("A schedule service call has occurred");
        return restActionFactory.getSchedule(ctx);
    };

}
