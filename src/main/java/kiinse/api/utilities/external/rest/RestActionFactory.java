package kiinse.api.utilities.external.rest;

import io.sentry.Sentry;
import kiinse.api.utilities.external.sql.SQL;
import kiinse.api.utilities.external.sql.SQLUtils;
import kiinse.api.utilities.external.redis.Redis;
import kiinse.api.utilities.format.Time;
import kiinse.api.utilities.parsing.ChangesParse;
import kiinse.api.utilities.parsing.TimeParse;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import services.moleculer.ServiceBroker;
import services.moleculer.context.Context;
import services.moleculer.transporter.RedisTransporter;

import java.io.IOException;
import java.sql.SQLException;
import java.text.SimpleDateFormat;

/**
 * Класс фабрики действий для Rest подключения
 * @author kiinse
 * @since 2.2.1
 * @version 2.2.4
 */
@Slf4j
public class RestActionFactory {

    /**
     * Метод, создающий данные о работе SQL
     * @return JsonObject с данными
     */
    public JSONObject getSQL() {
        var json = new JSONObject();
        json.put("chats_total", SQLUtils.totalChats());
        json.put("chats_blocked", SQLUtils.totalBlockedChats());
        json.put("chats_active", SQLUtils.totalActiveChats());
        json.put("chats_auto_mailing", SQLUtils.totalAutoMailing());
        json.put("chats_changes_mailing", SQLUtils.totalChangesMailing());
        json.put("chats_next_lesson", SQLUtils.totalNextLessonMailing());
        json.put("chats_ban", SQLUtils.totalBans());
        json.put("chats_admins", SQLUtils.allAdminsList().size());
        json.put("sql_login", SQL.getSQLSettings().getLogin());
        json.put("sql_host", SQL.getSQLSettings().getHost());
        json.put("sql_port", SQL.getSQLSettings().getPort());
        json.put("sql_db_name", SQL.getDataBaseName());
        try {
            json.put("sql_is_connection_closed", SQL.getConnection().isClosed());
        } catch (SQLException e) {
            log.warn("An error occurred while checking SQL connection. Message {}", e.getMessage());
            json.put("sql_is_connection_closed", true);
        }
        return json;
    }

    /**
     * Метод, создающий данные о работе Redis
     * @return JsonObject с данными
     */
    public JSONObject getRedis(ServiceBroker redisBroker, RedisTransporter redisTransporter) {
        var json = new JSONObject();
        json.put("redis_host", Redis.getRedisConnection().getHost());
        json.put("redis_port", Redis.getRedisConnection().getPort());
        json.put("redis_is_secure", redisTransporter.isSecure());
        json.put("redis_is_online", redisTransporter.isOnline(redisBroker.getNodeID()));
        json.put("redis_heartbeat_interval", redisTransporter.getHeartbeatInterval());
        json.put("redis_heartbeat_last", redisTransporter.getLastHeartbeatTime(redisBroker.getNodeID()));
        json.put("redis_heartbeat_timeout", redisTransporter.getHeartbeatTimeout());
        json.put("redis_cpu_usage", redisTransporter.getCpuUsage(redisBroker.getNodeID()));
        json.put("redis_name", redisTransporter.getName());
        json.put("redis_nodeID", redisBroker.getNodeID());
        json.put("redis_writers", redisBroker.getConfig().getJsonWriters());
        json.put("redis_readers", redisBroker.getConfig().getJsonReaders());
        return json;
    }

    /**
     * Метод, создающий данные о работе Бота
     * @return JsonObject с данными
     */
    public JSONObject getBot() {
        var time = new Time();
        var json = new JSONObject();
        var changesParse = new ChangesParse();
        var timeParse = new TimeParse();
        var rest = Rest.getRestConnection();
        if (rest.getBotVersion() != null) {
            json.put("bot_version", rest.getBotVersion());
        }
        if (rest.getApiVersion() != null) {
            json.put("api_version", rest.getApiVersion());
        }
        json.put("bot_time", new SimpleDateFormat("dd.MM.yyyy, HH:mm:ss z").format(time.getDate()));
        json.put("bot_weekday", time.getWeekday());
        json.put("bot_next_weekday", time.getNextWeekday());
        json.put("bot_subject", !time.isDenSubject() ? "Числитель" : "Знаменатель");
        json.put("bot_is_weekend", time.isWeekend(time.getWeekday()));
        json.put("bot_last_changes_parse", ChangesParse.getLastUpdate());
        json.put("bot_json_changes_date", changesParse.getChangesDateFromJson());
        json.put("bot_site_changes_date", changesParse.getChangesDate());
        json.put("bot_last_time_parse", TimeParse.getLastUpdate());
        json.put("sentry_is_enabled", Sentry.isEnabled());
        try {
            json.put("bot_is_changes_available", changesParse.isChangesAvailable());
        } catch (IOException e) {
            log.warn("An error occurred while checking availability of changes. Message {}", e.getMessage());
            json.put("bot_is_changes_available", false);
        }
        try {
            json.put("bot_is_time_available", timeParse.isTimeAvailable());
        } catch (IOException e) {
            log.warn("An error occurred while checking availability of call schedule. Message {}", e.getMessage());
            json.put("bot_is_time_available", false);
        }
        return json;
    }

    /**
     * Метод, создающий данные о заменах
     * @return JsonObject с данными
     */
    public JSONObject getChanges() {
        try {
            return (JSONObject) new JSONParser().parse(ChangesParse.getChangesParse().toString());
        } catch (ParseException e) {
            log.warn("An error occurred while parsing changes into json.simple. Message {}", e.getMessage());
            var json = new JSONObject();
            json.put("error", e.getMessage());
            return json;
        }
    }

    /**
     * Метод, создающий данные о расписании звонков
     * @return JsonObject с данными
     */
    public JSONObject getTime() {
        try {
            return (JSONObject) new JSONParser().parse(TimeParse.getTimeParse().toString());
        } catch (ParseException e) {
            log.warn("An error occurred while parsing time into json.simple. Message {}", e.getMessage());
            var json = new JSONObject();
            json.put("error", e.getMessage());
            return json;
        }
    }

    /**
     * Метод, создающий данные о расписании группы
     * @return JsonObject с данными
     */
    public JSONObject getSchedule(Context ctx) {
        try {
            return (JSONObject) new JSONParser().parse(new Redis().getSchedule(ctx.params.get("group", "null")).toString());
        } catch (ParseException e) {
            log.warn("An error occurred while parsing schedule into json.simple. Message {}", e.getMessage());
            var json = new JSONObject();
            json.put("error", e.getMessage());
            return json;
        }
    }
}
