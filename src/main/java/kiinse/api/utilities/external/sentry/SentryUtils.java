package kiinse.api.utilities.external.sentry;

import io.sentry.Sentry;
import lombok.extern.slf4j.Slf4j;

/**
 * Класс отвечающий за работу с Sentry
 *
 * @author kiinse
 * @since 1.0.21
 * @version 2.2.0
 */
@Slf4j
public class SentryUtils {

    /**
     * Метод отвечающий за инициализацию Sentry
     * @param connection Данные для подключения {@link SentryConnection}
     */
    public void init(SentryConnection connection) {
        Sentry.init(options -> {
            options.setDsn(connection.getDsn());
            options.setRelease("pkgh-telegram@" + connection.getRelease());
            options.setEnableAutoSessionTracking(true);
            options.setTracesSampleRate(1.0);
            options.setDebug(false);
        });
    }

    /**
     * Метод выключения Sentry
     */
    public void close() {
        if (Sentry.isEnabled()) {
            Sentry.close();
            log.info("Closing Sentry");
        } else {
            log.info("Sentry already closed");
        }
    }
}
