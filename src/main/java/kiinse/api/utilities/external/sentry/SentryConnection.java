package kiinse.api.utilities.external.sentry;

import kiinse.api.utilities.Utils;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

/**
 * Класс настроек для подключения к Sentry
 * @since 2.2.0
 * @version 2.2.1
 */
@Slf4j
public class SentryConnection {

    @Getter
    private String dsn = null;

    @Getter
    private String release = "?";

    /**
     * Setter для Sentry dsn
     * Default = null
     * @param sentryDsn Хост
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setDsn(String sentryDsn) throws IllegalArgumentException {
        if (Utils.isStringEmpty(sentryDsn)) {
            throw new IllegalArgumentException("Sentry dsn is empty");
        }
        this.dsn = sentryDsn;
        log.info("Sentry dsn set: {}", this.dsn.charAt(0) + "*****");
    }

    /**
     * Setter для Sentry release
     * Default = ?
     * @param sentryRelease Хост
     * @throws IllegalArgumentException Если строка пустая или null
     */
    public void setRelease(String sentryRelease) throws IllegalArgumentException {
        if (Utils.isStringEmpty(sentryRelease)) {
            throw new IllegalArgumentException("Sentry release is empty");
        }
        this.release = sentryRelease;
        log.info("Sentry release set: {}", this.release);
    }

}
