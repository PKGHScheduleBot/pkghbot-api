package kiinse.api.utilities.format;

import kiinse.api.utilities.parsing.TimeParse;
import lombok.Getter;
import lombok.Setter;

import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.Calendar;
import java.util.Date;
import java.util.TimeZone;

/**
 * Класс времени, для получения московской даты и различных обработок чего-то, что связано с временем
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.3.10
 */
public class Time {

    /** Enum Рабочие Дни/Выходные */
    public enum weekday {
        WEEKENDS,
        WORKINGDAYS
    }

    /**
     * Метод получения текущей даты
     *
     * @return Возвращает текущую Московскую дату
     */
    public Date getDate() {
        var nowUtc = Instant.now();
        var moscow = ZoneId.of("Europe/Moscow");
        return Date.from(ZonedDateTime.ofInstant(nowUtc, moscow).toInstant());
    }

    /**
     * Метод получения текущего календаря
     *
     * @return Возвращает текущий календарь Московской таймзоны
     */
    public Calendar getCalendar() {
        var cal = Calendar.getInstance();
        cal.setTimeZone(TimeZone.getTimeZone("Europe/Moscow"));
        return cal;
    }

    /**
     * Метод обработки дней недели. Если поступает Понедельник, то возвращает 0, если Вторник, то 1 и т.д.
     *
     * @param day День недели с большой буквы
     * @return Возвращает число
     */
    public int getWeekdayNumber(String day){
        return switch (day) {
            case "Понедельник" -> 0;
            case "Вторник" -> 1;
            case "Среда" -> 2;
            case "Четверг" -> 3;
            case "Пятница" -> 4;
            case "Суббота" -> 5;
            default -> 6;
        };
    }

    /**
     * Метод получения текущего дня недели.
     * @return Возвращает название текущего дня недели с большой буквы.
     */
    public String getWeekday(){
        var now = getDate();
        var day = new SimpleDateFormat("EEEEE").format(now);
        if (isCyrillic(day)) {
            return day.substring(0, 1).toUpperCase() + day.substring(1);
        } else {
            return getRussian(day);
        }
    }

    /**
     * Метод получения завтрашнего дня недели.
     *
     * @return Возвращает название завтрашнего дня недели с большой буквы.
     */
    public String getNextWeekday(){
        var now = getDate();
        var cal = getCalendar();
        cal.setTime(now);
        cal.add(Calendar.DATE, 1);
        var date = cal.getTime();
        var day = new SimpleDateFormat("EEEEE").format(date);
        if (isCyrillic(day)) {
            return day.substring(0, 1).toUpperCase() + day.substring(1);
        } else {
            return getRussian(day);
        }
    }

    /**
     * Метод для перевода дня недели с английского на русский
     * @param day день недели на английском языке
     * @return Возвращает день недели на русском языке с большой буквы
     */
    public String getRussian(String day){
        return switch (day.toLowerCase()) {
            case "monday" -> "Понедельник";
            case "tuesday" -> "Вторник";
            case "wednesday" -> "Среда";
            case "thursday" -> "Четверг";
            case "friday" -> "Пятница";
            case "saturday" -> "Суббота";
            default -> "Воскресенье";
        };
    }

    /**
     * Метод проверки на наличие кириллицы в строке
     * @param string Входная строка
     * @return True если есть кириллица
     */
    public boolean isCyrillic(String string) {
        for (char thisChar : string.toCharArray()) {
            if (Character.UnicodeBlock.of(thisChar) == Character.UnicodeBlock.CYRILLIC) {
                return true;
            }
        }
        return false;
    }

    /**
     * Метод проверки на субботу.
     *
     * @param day День недели
     * @return Возвращает Enum "Weekday.WEEKENDS" если суббота/воскресенье или "Weekday.WORKINGDAYS" если рабочий день.
     */
    public weekday isWeekend(String day) {
        var dayName = day.toLowerCase();
        return dayName.equals("суббота") || dayName.equals("воскресенье") ? weekday.WEEKENDS : weekday.WORKINGDAYS;
    }

    /**
     * Метод проверки Числитель/Знаменатель
     *
     * @return Возвращает True если знаменатель
     */
    public boolean isDenSubject() {
        return getWeekday().equals("Воскресенье") != ((getCalendar().get(Calendar.WEEK_OF_MONTH) % 2) == 0);
            /*
                false - числитель
                true - знаменатель
                По воскресеньям наоборот (Для нормальной работы рассылки, чтобы учитывала, что уже неделя закончилась)
            */
    }

    /**
     * Метод получения расписания звонков по номеру пары
     *
     * @param day Enum, показывающий на какой тип дня брать расписание. {@link weekday}
     * @param lesson Номер пары
     * @return Возвращает расписание звонков определённой пары
     */
    public String getTime(weekday day, int lesson) {
        return switch (day) {
            case WEEKENDS -> TimeParse.getTimeParse().getJSONObject(String.valueOf(lesson)).getString("Выходной");
            case WORKINGDAYS -> TimeParse.getTimeParse().getJSONObject(String.valueOf(lesson)).getString("Рабочий День");
        };
    }
}
