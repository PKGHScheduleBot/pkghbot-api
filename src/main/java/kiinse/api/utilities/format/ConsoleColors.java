package kiinse.api.utilities.format;

/**
 * Класс формата текста в цвет
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.1.0
 */
public class ConsoleColors {

    /**
     * Enum с названием цветов. RED, GREEN, YELLOW
     */
    public enum color {
        RED,
        GREEN,
        YELLOW
    }

    /**
     * Метод форматирования текста в нужный цвет для консоли
     *
     * @param color Нужный цвет {@link color}
     * @param text Поступающий текст
     * @return Отформатированный текст
     */
    public static String color(color color, String text){
        return switch (color) {
            case RED -> String.format("\u001B[31m%s\u001B[0m", text);
            case GREEN -> String.format("\u001B[32m%s\u001B[0m", text);
            case YELLOW -> String.format("\\u001b[33m%s\u001B[0m", text);
        };
    }
}
