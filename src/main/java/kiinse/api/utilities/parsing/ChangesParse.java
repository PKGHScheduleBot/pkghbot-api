package kiinse.api.utilities.parsing;

import io.sentry.Sentry;
import kiinse.api.utilities.Utils;
import kiinse.api.utilities.format.Time;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang.StringUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Класс парсинга замен с сайта колледжа pkgh.edu.ru
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.2.10
 */
@Slf4j
public class ChangesParse {

    /** Json свойство, хранящее список замен */
    @Getter
    private static JSONObject changesParse = new JSONObject();

    /** Свойство, хранящее дату последнего обновления */
    @Getter
    private static String lastUpdate = "N/A";

    /**
     * Метод, который парсит замены с сайта в свойство <b>changesParse</b>
     * @return Возвращает лист с списком обновлённых групп
     */
    public List<String> parse() {
        List<String> groups = new ArrayList<>();
        var time = new Time();
        var oldChanges = new JSONObject(changesParse.toString());
        try {
            if (isChangesAvailable()) {
                var now = time.getDate();
                var sdf = new SimpleDateFormat("d.MM, HH:mm");
                var doc = Jsoup.connect("https://pkgh.edu.ru/obuchenie/shedule-of-classes.html").get();
                changesParse.keySet().clear();
                var table = doc.getElementsByClass("shedule changes");
                for (Element row : table.select("tr")) {
                    var innerJson = new JSONObject();
                    var outerJson = new JSONObject();
                    var tds = row.select("td");
                    if (tds.text().length() > 1) {
                        var group = tds.get(0).text();
                        var num = tds.get(1).text();
                        var lesson = tds.get(2).text();
                        var change = tds.get(3).text();
                        innerJson.put("По расписанию", hasLesson(lesson));
                        innerJson.put("Замена", change);
                        if (changesParse.has(group)) {
                            try {
                                if (changesParse.getJSONObject(group).has(num)) {
                                    var copInnerJson = new JSONObject();
                                    copInnerJson.put("По расписанию", changesParse.getJSONObject(group).getJSONObject(num).get("По расписанию") + "\n------\n" + hasLesson(lesson));
                                    copInnerJson.put("Замена", changesParse.getJSONObject(group).getJSONObject(num).get("Замена") + "\n------\n" + hasLesson(change));
                                    outerJson = changesParse.getJSONObject(group);
                                    outerJson.remove(num);
                                    outerJson.put(num, copInnerJson);

                                } else {
                                    outerJson = changesParse.getJSONObject(group);
                                    outerJson.put(num, innerJson);
                                }
                                changesParse.remove(group);
                                changesParse.put(group, outerJson);
                            } catch (JSONException e) {
                                log.warn("An error occurred while writing json during parsing: {}", e.getMessage());
                                Sentry.captureException(e);
                            }
                        } else {
                            outerJson.put(num, innerJson);
                            changesParse.put(group, outerJson);
                        }
                    }
                }
                log.info("Changes updated");
                var oldChangesString = oldChanges.toString();
                if ((!Objects.equals(oldChangesString, "{}") && !Utils.isStringEmpty(oldChangesString))) {
                    changesParse.keySet().forEach(keyStr ->
                    {
                        if (!oldChanges.has(keyStr) || (oldChanges.has(keyStr) && !changesParse.has(keyStr)) || !Objects.equals(oldChanges.getJSONObject(keyStr).toString(), changesParse.getJSONObject(keyStr).toString())) {
                            groups.add(keyStr);
                        }

                    });
                    log.info("Groups changes [{}] has been changed", StringUtils.join(groups, ", "));
                }
                changesParse.put("Дата", getChangesDate());
                lastUpdate = sdf.format(now);
            }
        } catch (IOException e) {
            log.error("An error occurred while parsing changes: {}", e.getMessage());
            Sentry.captureException(e);
        }
        return groups;
    }

    /**
     * Метод для проверки сайта на доступность таблицы с заменами.
     *
     * @return True если сайт доступен и в таблице замен есть текст.
     * @throws IOException Ошибка в случае недоступности страницы сайта
     */
    public boolean isChangesAvailable() throws IOException {
        var doc = Jsoup.connect("https://pkgh.edu.ru/obuchenie/shedule-of-classes.html").get();
        return doc.toString() != null && doc.getElementsByClass("shedule changes").hasText();
    }

    /**
     * Данный метод по получению даты с сайта колледжа, на которую вывешены замены.
     * @return Дата, на которую вывешены замены
     */
    public String getChangesDate() {
        try {
            var doc = Jsoup.connect("https://pkgh.edu.ru/obuchenie/shedule-of-classes.html").get();
            if (doc.toString() != null) {
                var elements = doc.getElementsByClass("custom otherpadding changes");
                return elements.select("h4").text().replace(" ", "").replace("Заменына", "").substring(0, 5);
            }
        } catch (IOException e) {
            log.error("An error occurred while parsing the changes date: {}", e.getMessage());
            Sentry.captureException(e);
        }
        return "N/A";
    }

    /**
     * Данный метод по получению даты с json свойства с заменами, на которую вывешены замены.
     * @return Дата, на которую вывешены замены
     */
    public String getChangesDateFromJson() {
        String date = "N/A";
        try {
            date = changesParse.getString("Дата");
        } catch (Exception e) {
            log.warn("The date of the last update of changes was not found in json. Message: {}", e.getMessage());
            Sentry.captureException(e);
        }
        return date;
    }

    /**
     * Обработчик замен. Убирает большие тире в пару маленьких.
     * @param text Необработанное сообщение
     * @return Обработанное сообщение
     */
    private String hasLesson(String text) {
        if (text.contains("-------------------")) {
            return "------";
        }
        return text;
    }

    /**
     * Данный метод берёт из Json файла замены по паре.
     *
     * @param groupName Имя группы
     * @param lesson Номер пары
     * @return Информация по определённой паре
     */
    public JSONObject getChangesByLesson(String groupName, int lesson) {
        return getChangesParse().getJSONObject(groupName).getJSONObject(String.valueOf(lesson));
    }

    /**
     * Данный метод обрабатывает json свойство в читаемый вид.
     *
     * @param groupName Имя группы
     * @return Обработанное сообщение, готовое к отправке
     */
    public String getAllChanges(String groupName) {
        var str = new StringBuilder();
        str.append("\n<b>▬▬▬▬ Замены на ").append(getChangesDateFromJson()).append(" ▬▬▬▬</b>").append("\nГруппа: ").append(groupName);
        if (hasChanges(groupName)) {
            for (int i = 1; i <= 7; i++) {
                if (hasChangesByLesson(groupName, i)) {
                    JSONObject json = getChangesByLesson(groupName, i);
                    str.append("\n\n<u>→ Пара №").append(i).append(" ←</u>\n");
                    str.append("<b>По расписанию:</b> ").append(json.getString("По расписанию")).append("\n");
                    str.append("<b>Замена:</b> ").append(json.getString("Замена"));
                }
            }
        } else {
            str.append("\n\nЗамен нет :(");
        }
        str.append("\n\n→ Последнее обновление: ").append(getLastUpdate());
        return str.toString();
    }

    /**
     * Данный метод возвращает True если в json свойстве у указанной группы есть замены.
     *
     * @param groupName Имя группы
     * @return Есть ли замены
     */
    public Boolean hasChanges(String groupName) {
        return getChangesParse().has(groupName);
    }

    /**
     * Данный метод возвращает True если в json свойстве у указанной группы есть замены указанной пары.
     *
     * @param groupName Имя группы
     * @param lesson Номер пары
     * @return Есть ли замены
     */
    public Boolean hasChangesByLesson(String groupName, int lesson) {
        if (getChangesParse().has(groupName)) {
            return getChangesParse().getJSONObject(groupName).has(String.valueOf(lesson));
        }
        return false;
    }
}
