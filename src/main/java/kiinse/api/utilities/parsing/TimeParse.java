package kiinse.api.utilities.parsing;

import io.sentry.Sentry;
import kiinse.api.utilities.data.TimeHashMap;
import kiinse.api.utilities.format.Time;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONObject;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import java.io.IOException;
import java.text.SimpleDateFormat;

/**
 * Класс парсинга времени с сайта колледжа pkgh.edu.ru
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.2.1
 */
@Slf4j
public class TimeParse {

    /** Json свойство, хранящее расписание звонков */
    @Getter
    private static JSONObject timeParse = new JSONObject();

    /** Свойство, хранящее дату последнего обновления */
    @Getter
    private static String lastUpdate = "N/A";

    /** Метод, который парсит расписание звонков с сайта в свойство <b>json</b> */
    public void parse() {
        var timeHashMap = new TimeHashMap();
        var time = new Time();
        try {
            if (isTimeAvailable()) {
                timeParse.keySet().clear();
                var now = time.getDate();
                var sdf = new SimpleDateFormat("d.MM, HH:mm");
                var doc = Jsoup.connect("https://pkgh.edu.ru/obuchenie/shedule-of-classes.html").get();
                var calls_table = doc.getElementsByClass("simple-little-table");
                for (Element row : calls_table.select("tr")) {
                    var jsonTime = new JSONObject();
                    var tds = row.select("td");
                    if (tds.text().length() > 1) {
                        jsonTime.put("Рабочий День", timeFormat(tds.get(1).text()));
                        jsonTime.put("Выходной", timeFormat(tds.get(2).text()));
                        timeParse.put(tds.get(0).text(), jsonTime);
                    }
                }
                lastUpdate = sdf.format(now);
                timeHashMap.TimeHashMapInitialize();
                log.info("Time updated");
            }
        } catch (IOException e) {
            log.error("An error occurred while parsing time: {}", e.getMessage());
            Sentry.captureException(e);
        }
    }

    /**
     * Метод для проверки сайта на доступность таблицы с расписанием.
     *
     * @return True если сайт доступен и в таблице расписания есть текст.
     * @throws IOException Ошибка в случае недоступности страницы сайта
     */
    public boolean isTimeAvailable() throws IOException {
        var doc = Jsoup.connect("https://pkgh.edu.ru/obuchenie/shedule-of-classes.html").get();
        return doc.toString() != null && doc.getElementsByClass("simple-little-table").hasText();
    }

    /**
     * Метод обработки полученного времени с сайта в удобный вид
     * @param Time необработанное время
     * @return Возвращает обработанное время в String формате
     */
    public String timeFormat(String Time) {
        var time = Time.replace(" ", "");
        if (time.length() <= 2) {
            return "-";
        }
        return String.valueOf(
                time.charAt(0)) +
                time.charAt(1) +
                ":" +
                time.charAt(2) +
                time.charAt(3) +
                "-" +
                time.charAt(5) +
                time.charAt(6) +
                ":" +
                time.charAt(7) +
                time.charAt(8);
    }
}
