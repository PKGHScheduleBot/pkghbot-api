package kiinse.api.utilities.files;

import io.sentry.Sentry;
import kiinse.api.utilities.files.utils.FilesPathes;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Класс взятия текста из файла PKGHSchedule/splashes.json
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.0.0
 */
@Slf4j
public class Splashes {

    /**
     * Метод взятия случайного splash текста из файла.
     *
     * @return Взятый текст
     */
    public static String getSplash() {
        try {
            var jsonFile = (JSONObject) new JSONParser().parse(new InputStreamReader(new FileInputStream(FilesPathes.getPath(FilesPathes.files.SPLASHES, FilesPathes.types.JSON)), StandardCharsets.UTF_8));
            int random = 1 + (int) (Math.random() * jsonFile.size());
            return jsonFile.get(String.valueOf(random)).toString();
        } catch (IOException | ParseException e) {
            log.warn("An error occurred while getting data from file: {}", e.getMessage());
            Sentry.captureException(e);
        }
        return "Ошибка в получении текста.";
    }
}
