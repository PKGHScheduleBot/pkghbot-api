package kiinse.api.utilities.files;

import io.sentry.Sentry;
import kiinse.api.utilities.files.utils.FilesPathes;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Класс получения текста из PKGHSchedule/text.json
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.0.0
 */
@Slf4j
public class Text {

    /** Enum, содержащий названия необходимых шаблонов текста */
    public enum text {
        SETTINGS,
        START,
        SCHEDULE,
        ADDGROUP,
        EDITGROUP,
        FIRSTSTART,
        ADMINCOMMANDS,
        ADMINSETTINGS,
        SCHEDULELIST,
        USERCOMMANDS
    }

    /**
     * Метод взятия текста из файла
     *
     * @param isGroup Boolean значение, определяющее текст. В группу или в личку
     * @param text Enum, определяющий шаблон {@link text}
     * @return Возвращает текст из файла
     */
    public static String getText(Boolean isGroup, text text) {
        try {
            var jsonFile = (JSONObject) new JSONParser().parse(new InputStreamReader(new FileInputStream(FilesPathes.getPath(FilesPathes.files.TEXT, FilesPathes.types.JSON)), StandardCharsets.UTF_8));
            var textFromJson = new org.json.JSONObject(isGroup ? jsonFile.get("group").toString() : jsonFile.get("user").toString());
            return textFromJson.getString(text.toString().toLowerCase());
        } catch (IOException | ParseException e) {
            log.warn("An error occurred while getting data from file: {}", e.getMessage());
            Sentry.captureException(e);
        }
        return null;
    }
}
