package kiinse.api.utilities.files;

import io.sentry.Sentry;
import kiinse.api.utilities.files.utils.FilesPathes;
import lombok.extern.slf4j.Slf4j;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.nio.charset.StandardCharsets;

/**
 * Класс получения сообщений из PKGHSchedule/messages.json
 *
 * @author kiinse
 * @since 2.0.0
 * @version 2.2.0
 */
@Slf4j
public class Messages {

    /**
     * Enum, содержащий необходимые сообщения
     */
    public enum messages {
        NOPERMISSIONSMSG,
        BANMSG,
        UNBANMSG,
        BANTOADMINMSG,
        YOUBANNEDMSG,
        SPAMALERTMSG,
        GROUPSAVEDMSG,
        GROUPNOTFOUNDMSG,
        ERRORLESSONSMSG,
        EMPTYMESSAGEMSG,
        USERNOTFOUNDMSG,
        ADMINREMOVEDMSG,
        ADMINADDEDMSG
    }

    /**
     * Метод взятия сообщений из файла
     *
     * @param msg Enum, определяющий сообщение {@link messages}
     * @return Возвращает сообщение из файла
     */
    public static String getText(messages msg) {
        try {
            var jsonFile = (JSONObject) new JSONParser().parse(new InputStreamReader(new FileInputStream(FilesPathes.getPath(FilesPathes.files.MESSAGES, FilesPathes.types.JSON)), StandardCharsets.UTF_8));
            return jsonFile.get(msg.toString().toLowerCase()).toString();
        } catch (IOException | ParseException e) {
            log.warn("An error occurred while getting data from file: {}", e.getMessage());
            Sentry.captureException(e);
        }
        return null;
    }
}
