package kiinse.api.utilities.files.utils;

import java.io.File;

/**
 * Класс работы с путями файлов
 *
 * @author kiinse
 * @since 2.0.0
 * @version 2.0.0
 */
public class FilesPathes {

    /**
     * Константа с названием папки для файлов бота
     */
    public static final String FOLDER = "PKGHBotFiles";

    /**
     * Enum, содержащий имена файлов
     */
    public enum files {
        SPLASHES,
        CONFIG,
        TEXT,
        SPONSOR,
        MESSAGES
    }

    /**
     * Enum, содержащий типы файлов
     */
    public enum types {
        JAR,
        TXT,
        JSON,
        PROPERTIES,
        YML,
        BSON
    }

    /**
     * Метод, позволяющий получить путь для необходимого файла
     * @param fileName Имя файла {@link files}
     * @param fileType Тип файла {@link types}
     * @return String путь, формата PKGHBotFiles/файл.тип
     */
    public static String getPath(files fileName, types fileType) {
        return FOLDER + File.separator + fileName.toString().toLowerCase() + "." + fileType.toString().toLowerCase();
    }

    /**
     * Метод, позволяющий получить необходимый файл в папке PKGHBotFiles
     * @param fileName Имя файла {@link files}
     * @param fileType Тип файла {@link types}
     * @return Возвращает указанный файл
     */
    public static File getFile(files fileName, types fileType) {
        return new File(FOLDER + File.separator + fileName.toString().toLowerCase() + "." + fileType.toString().toLowerCase());
    }
}
