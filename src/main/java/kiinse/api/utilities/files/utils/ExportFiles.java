package kiinse.api.utilities.files.utils;

import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;

/**
 * Класс создания файлов в папку PKGHSchedule из resources внутри jar
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.2.0
 */
@Slf4j
public class ExportFiles {

    /**
     * Метод, который копирует файлы из resources внутри jar
     * @throws IOException Ловит ошибки при невозможности получения доступа к файлам.
     */
    public void export() throws IOException{
        copyFile(accessFile("splashes.json"), FilesPathes.getFile(FilesPathes.files.SPLASHES, FilesPathes.types.JSON));
        copyFile(accessFile("text.json"), FilesPathes.getFile(FilesPathes.files.TEXT, FilesPathes.types.JSON));
        copyFile(accessFile("sponsor.json"), FilesPathes.getFile(FilesPathes.files.SPONSOR, FilesPathes.types.JSON));
        copyFile(accessFile("messages.json"), FilesPathes.getFile(FilesPathes.files.MESSAGES, FilesPathes.types.JSON));
    }

    /**
     * Метод для чтения файлов внутри jar.
     *
     * @param resource Имя нужного файла
     * @return Возвращает InputStream указанного файла
     */
    public static InputStream accessFile(String resource) {
        InputStream input = ExportFiles.class.getResourceAsStream(File.separator + "resources" + File.separator + resource);
        if (input == null) {
            input = ExportFiles.class.getClassLoader().getResourceAsStream(resource);
        }
        return input;
    }

    /**
     * Метод копирования файлов
     * @param sourceFile Входной файл
     * @param destFile Выходной файл
     * @throws IOException Ловит ошибки при невозможности получения доступа к файлам.
     */
    private static void copyFile(InputStream sourceFile, File destFile) throws IOException {
        if (! destFile.exists()) {
            FileUtils.copyInputStreamToFile(sourceFile, destFile);
            log.info("File '{}' created", destFile);
        } else {
            log.info("File '{}' loaded", destFile);
        }
    }
}

