package kiinse.api.utilities;

import kiinse.api.utilities.external.redis.Redis;
import kiinse.api.utilities.files.Messages;
import kiinse.api.utilities.format.Time;
import kiinse.api.utilities.parsing.ChangesParse;
import lombok.extern.slf4j.Slf4j;
import org.json.JSONException;
import org.json.JSONObject;
import java.util.Arrays;
import java.util.Objects;

/**
 * Класс утилит
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.2.10
 */
@Slf4j
public class Utils {

    private final Redis moleculer = new Redis();
    private final Time time = new Time();

    /**
     * Enum настроек для указания в методах
     */
    public enum settings {
        AUTOMAILING,
        NONE,
        CHANGES
    }

    /**
     * Enum числитель пара / знаменатель пара
     */
    public enum subject {
        numSubject,
        denSubject
    }

    /**
     * Enum числитель преподаватель / знаменатель преподаватель
     */
    public enum teacher {
        numTeacher,
        denTeacher
    }

    /**
     * Enum расписание на неделю или на день
     */
    public enum messageFormat {
        WEEKLY,
        DAILY
    }

    /**
     * Метод проверки существования группы на сайте
     *
     * @param group Получаемое название группы
     * @return Возвращает true если parser не отдал null и группа имеет правильный написанный формат XX-XX-XX или XX-XX-X
     */
    public boolean checkGroup(String group) {
        return moleculer.getSchedule(group) != null && String.valueOf(group.charAt(2)).equals("-") && String.valueOf(group.charAt(5)).equals("-");
    }

    /**
     * Метод получения, обработки и форматирования расписания в читаемый вид
     *
     * @param group Группа пользователя
     * @param dayName День недели
     * @param settings Получаемые настройки. К примеру: Добавлять ли замены, Рассылка или нет {@link settings}
     * @return Возвращает либо расписание, либо сообщение о том, что произошла ошибка с прикреплёнными заменами
     */
    public String getLessons(String group, String dayName, settings[] settings) {
        var changes = new ChangesParse();
        var json = moleculer.getSchedule(group);
        if (json != null && group.equals(json.getString("name"))) {
            var day = isDay(dayName.replace("/", "").substring(0, 1).toUpperCase() + dayName.replace("/", "").substring(1));
            var msg = Arrays.asList(settings).contains(Utils.settings.AUTOMAILING) ? new StringBuilder("▒▒ Вечерняя рассылка ▒▒\n" + "#Расписание " + json.getString("name")) : new StringBuilder("#Расписание " + json.getString("name"));
            if (day.equals("week")) {
                    msg
                            .append(msgBuilder(json.getJSONArray("table").getJSONObject(0), "Понедельник", messageFormat.WEEKLY))
                            .append(msgBuilder(json.getJSONArray("table").getJSONObject(1), "Вторник", messageFormat.WEEKLY))
                            .append(msgBuilder(json.getJSONArray("table").getJSONObject(2), "Среда", messageFormat.WEEKLY))
                            .append(msgBuilder(json.getJSONArray("table").getJSONObject(3), "Четверг", messageFormat.WEEKLY))
                            .append(msgBuilder(json.getJSONArray("table").getJSONObject(4), "Пятница", messageFormat.WEEKLY))
                            .append(msgBuilder(json.getJSONArray("table").getJSONObject(5), "Суббота", messageFormat.WEEKLY));
                } else {
                    if (!day.equals("Воскресенье")) {
                        msg.append(msgBuilder(json.getJSONArray("table").getJSONObject(time.getWeekdayNumber(day)), day, messageFormat.DAILY));
                    } else {
                        msg.append("\nВообще-то в этот день воскресенье :3");
                    }
                    if (Arrays.asList(settings).contains(Utils.settings.CHANGES)) {
                        msg.append(changes.getAllChanges(group));
                    }
            }
            return msg.toString();
        } else {
            log.warn("An error occurred while sending the schedule. Received group: {} | User group: {}", (json != null ? json.getString("name") : "null"), group);
            return Objects.requireNonNull(Messages.getText(Messages.messages.ERRORLESSONSMSG)).replace("ERRORGROUP", (json != null ? json.getString("name") : "null")).replace("GROUP", group).replace("CHANGES", changes.getAllChanges(group));
        }
    }

    /**
     * Метод обработки полученной команды в название дня недели
     * @param day Полученное значение (К примеру: "Завтра")
     * @return Возвращает название дня недели (к примеру "Понедельник") или "week" для всей недели
     */
    private String isDay(String day) {
        return switch (day) {
            case "Сегодня" -> time.getWeekday();
            case "Завтра" -> time.getNextWeekday();
            case "Вся неделя📅", "Расписание" -> "week";
            default -> day;
        };
    }

    /**
     * Метод обработки расписания (Определённый день недели)
     *
     * @param lesson Необработанный JSON объект с указанной группой и парами
     * @param day День недели
     * @param format Вид расписания недельная или обычный {@link messageFormat}
     * @return Возвращает StringBuilder с обработанным расписанием определённого дня недели в читаемом виде
     */
    public StringBuilder msgBuilder(JSONObject lesson, String day, messageFormat format) {
        var msg = new StringBuilder("\n<b>▬▬▬▬ " + day + " ▬▬▬▬</b>\n");
        var lessons = new StringBuilder();
        lessons
                .append(lesson(1, lesson, day, format))
                .append(lesson(2, lesson, day, format))
                .append(lesson(3, lesson, day, format))
                .append(lesson(4, lesson, day, format))
                .append(lesson(5, lesson, day, format))
                .append(lesson(6, lesson, day, format));
        if (lessons.toString().replace(" ", "").length() == 0) {
            return msg.append("\nВ этот день пар нет :3\n");
        }
        return msg.append(lessons);
    }


    /**
     * Метод обработки пары в дне недели
     *
     * @param num Номер пары
     * @param lesson Необработанный JSON объект с указанной группой и парами
     * @param day День недели
     * @param format Вид расписания недельная или обычный {@link messageFormat}
     * @return Возвращает StringBuilder с обработанной определённой парой
     */
    public StringBuilder lesson(int num, JSONObject lesson, String day, messageFormat format) {
        var msg = new StringBuilder();
        JSONObject para;
        try {
            para = lesson.getJSONArray("lesson").getJSONObject(num);
        } catch (JSONException e) {
            return msg;
        }
        if (hasLesson(para)) {
            msg.append("\n<u>→ Пара №").append(num).append(" | ").append(time.getTime(time.isWeekend(day), num)).append(" ←</u>\n");
            if (isNumSubject(para, format)) {
                if (format.equals(messageFormat.WEEKLY)) {
                    msg.append(getWeekLesson(subject.denSubject, subject.numSubject, teacher.numTeacher, para));
                } else {
                    msg.append(getLesson(subject.numSubject, teacher.numTeacher, para));
                }
            }
            if (isDenSubject(para, format)) {
                if (format.equals(messageFormat.WEEKLY)) {
                    msg.append(getWeekLesson(subject.numSubject, subject.denSubject, teacher.denTeacher, para));
                } else {
                    msg.append(getLesson(subject.denSubject, teacher.denTeacher, para));
                }
            }
        }
        return msg;
    }

    /**
     * Проверка на наличие пары
     * @param lesson JSONObject с данными определённой пары
     * @return true если есть numSubject или denSubject
     */
    public boolean hasLesson(JSONObject lesson) {
        return !isStringEmpty(lesson.getString("numSubject")) || !isStringEmpty(lesson.getString("denSubject"));
    }

    /**
     * Проверка если есть numSubject и сверка с числитель/знаменатель и вид расписания недельная или обычный
     * @param lesson JSONObject с данными определённой пары
     * @param format Вид расписания недельная или обычный {@link messageFormat}
     * @return true если numSubject не пуст и если числитель или неделя и пуст denSubject
     */
    public boolean isNumSubject(JSONObject lesson, messageFormat format) {
        return (!isStringEmpty(lesson.getString("numSubject")) && (!time.isDenSubject() || format.equals(messageFormat.WEEKLY))) || isStringEmpty(lesson.getString("denSubject"));
    }

    /**
     * Проверка если есть denSubject и сверка с числитель/знаменатель и вид расписания недельная или обычный
     * @param lesson JSONObject с данными определённой пары
     * @param format Вид расписания недельная или обычный {@link messageFormat}
     * @return true если denSubject не пуст и если числитель или неделя и пуст numSubject
     */
    public boolean isDenSubject(JSONObject lesson, messageFormat format) {
        return (!isStringEmpty(lesson.getString("denSubject")) && (time.isDenSubject() || format.equals(messageFormat.WEEKLY))) || isStringEmpty(lesson.getString("numSubject"));
    }

    /**
     * Тут я даже описывать не хочу, ибо это тупо пиздец.
     */
    private StringBuilder getWeekLesson(subject checkSubject, subject subject, teacher teacher, JSONObject lesson) {
        StringBuilder msg = new StringBuilder();
        if (subject.toString().equals("denSubject")) {
            msg.append("-------------------\n");
        }
        boolean isDenSubject = subject.equals(Utils.subject.numSubject) != time.isDenSubject();
        if (isDenSubject && !isStringEmpty(lesson.getString(checkSubject.toString()))) {
            msg.append(" →").append(lesson.getString(subject.toString())).append("\n");
            if (!isStringEmpty(lesson.getString(teacher.toString()))) {
                msg.append(" →").append(lesson.getString(teacher.toString())).append("\n");
            }
        } else {
            msg.append(getLesson(subject, teacher, lesson));
        }
        return msg;
    }

    /**
     * Форматирует предмет и преподавателя в подобающий вид
     * @param subject Предмет numSubject / denSubject {@link subject}
     * @param teacher Преподаватель numTeacher / denTeacher {@link teacher}
     * @param lesson JSONObject с данными определённой пары
     * @return Возвращает данные в приятном для глаза виде
     */
    private StringBuilder getLesson(subject subject, teacher teacher, JSONObject lesson) {
        StringBuilder msg = new StringBuilder();
        msg.append(lesson.getString(subject.toString())).append("\n");
        if (!isStringEmpty(lesson.getString(teacher.toString()))) {
            msg.append(lesson.getString(teacher.toString())).append("\n");
        }
        return msg;
    }

    /**
     * Метод проверки строки на наличие данных
     * @param string поступающая строка
     * @return возвращает true если строка null или содержит одни пробелы
     */
    public static boolean isStringEmpty(String string) {
        return string == null || string.replaceAll(" ", "").replaceAll(" ", "").isEmpty();
    }

}
