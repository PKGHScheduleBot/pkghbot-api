package kiinse.api.utilities.data;

import io.sentry.Sentry;
import kiinse.api.utilities.format.Time;
import kiinse.api.utilities.parsing.TimeParse;
import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;

/**
 * Класс, содержащий в себе HashMap для получения номера пары по времени
 *
 * @author kiinse
 * @since 1.0.0
 * @version 2.2.1
 */
@Slf4j
public class TimeHashMap {


    /** HashMap, содержащий расписание звонков в будние дни */
    @Getter
    private static final HashMap<String, Integer> normalMap = new HashMap<>();

    /** HashMap, содержащий расписание звонков в выходные дни */
    @Getter
    private static final HashMap<String, Integer> weekendMap = new HashMap<>();

    /**
     * Метод инициализации HashMap
     */
    public void TimeHashMapInitialize(){
        var time = new Time();
        try {
            normalMap.clear();
            weekendMap.clear();
            normalMap.put(time.getTime(Time.weekday.WORKINGDAYS, 1).substring(0, 3) + "00", 1);
            weekendMap.put(time.getTime(Time.weekday.WEEKENDS, 1).substring(0, 3) + "00", 1);
            normalMap.put(formatTime(time.getTime(Time.weekday.WORKINGDAYS, 1)), 2);
            weekendMap.put(formatTime(time.getTime(Time.weekday.WEEKENDS, 1)), 2);
            normalMap.put(formatTime(time.getTime(Time.weekday.WORKINGDAYS, 2)), 3);
            weekendMap.put(formatTime(time.getTime(Time.weekday.WEEKENDS, 2)), 3);
            normalMap.put(formatTime(time.getTime(Time.weekday.WORKINGDAYS, 3)), 4);
            weekendMap.put(formatTime(time.getTime(Time.weekday.WEEKENDS, 3)), 4);
            normalMap.put(formatTime(time.getTime(Time.weekday.WORKINGDAYS, 4)), 5);
            weekendMap.put(formatTime(time.getTime(Time.weekday.WEEKENDS, 4)), 5);
            normalMap.put(formatTime(time.getTime(Time.weekday.WORKINGDAYS, 5)), 6);
            weekendMap.put(formatTime(time.getTime(Time.weekday.WEEKENDS, 5)), 6);
            normalMap.put(formatTime(time.getTime(Time.weekday.WORKINGDAYS, 6)), 7);
            weekendMap.put(formatTime(time.getTime(Time.weekday.WEEKENDS, 6)), 7);
        } catch (Exception e) {
            log.warn("An error occurred while initializing HashMap time: {}", e.getMessage());
            Sentry.captureException(e);
        }
    }

    /**
     * Метод форматирования времени
     * @param time Необработанное время
     * @return Отформатированное время
     */
    private String formatTime(String time) {
        return time.length() > 1 ? time.substring(6) : time;
    }

}
